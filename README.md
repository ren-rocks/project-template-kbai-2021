## Project template - kbai spring 2021

A container to run every project in KBAI Sprint 2021.

### Requirements
- [docker](https://docs.docker.com/desktop/)

that is it.

### Installation Instructions
#### VSCode
1. clone this repo
2. open this repo on vscode
3. the build task should execute. If the build task doesn't execute you run task `build`

### Terminal
1. clone this repo
2. from within the repo's directory, run `docker build -t kbai .`

### Running project
add the project code into the app directory. The current code within the app directory is based on [mini-project-2](http://lucylabs.gatech.edu/kbai/spring-2021/mini-project-2/), but you can replace the content of the app directory with any task given in KBAI 2021

- run vscode task `run`
or
- run `docker run -v ${workspaceFolder}/app:/etc/app -t kbai`
the output will be returned on the terminal

#### Submission
To submit, run the zip task or `zip -z ${workspaceFolder}/project.zip app/*`. This will create a `project.zip` file for submission. 
